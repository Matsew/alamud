# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event3


class TirerOnEvent(Event3):
    NAME = "tirer-on"

    def perform(self):
        self.inform("tirer-on")
